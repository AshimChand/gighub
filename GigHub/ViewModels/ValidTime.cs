﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace GigHub.ViewModels
{
    public class ValidTime:ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            DateTime datetime;
            var isvalid = DateTime.TryParseExact(value.ToString(),
                   "HH:mm",
                   CultureInfo.CurrentCulture,
                   DateTimeStyles.None,
                   out datetime);
            return (isvalid);
        }
    }
}
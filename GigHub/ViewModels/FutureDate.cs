﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace GigHub.ViewModels
{
    public class FutureDate:ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            DateTime datetime;
         var isvalid=   DateTime.TryParseExact(value.ToString(), "d MMM yyyy",
                CultureInfo.CurrentCulture,
                DateTimeStyles.None,
                out datetime);
            return (isvalid && datetime > DateTime.Now);
        }
    }
}
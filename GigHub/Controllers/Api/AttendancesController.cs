﻿using GigHub.DTO;
using GigHub.Models;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Web.Http;

namespace GigHub.Controllers.Api
{
    [Authorize]
    public class AttendancesController : ApiController
    {
        private ApplicationDbContext _context;
        public AttendancesController()
        {
            _context = new ApplicationDbContext();
        }


        [HttpPost]
        public IHttpActionResult Attend(AttendanceDto dto)
        {
            var userid = User.Identity.GetUserId();
            if (_context.Attendances.Any(a => a.GigId == dto.GigId && a.AttendeeId == userid))
                return BadRequest("The Attendance already Exist");
            var attendance = new Attendance
            {
                AttendeeId = User.Identity.GetUserId(),
                GigId = dto.GigId
            };
            _context.Attendances.Add(attendance);
            _context.SaveChanges();
            return Ok();
        }
    }
}

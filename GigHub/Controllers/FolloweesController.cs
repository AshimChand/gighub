﻿using GigHub.Models;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Web.Mvc;

namespace GigHub.Controllers
{
    public class FolloweesController : Controller
    {
        private ApplicationDbContext _context;
        public FolloweesController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: Followees
        public ActionResult Index()
        {
            var userid = User.Identity.GetUserId();
            var artist = _context.Followings.Where(a => a.FolloweeId == userid)
                .Select(a => a.Follower)
                .ToList();
            return View(artist);
        }
    }
}
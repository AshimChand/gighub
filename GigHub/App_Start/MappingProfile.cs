﻿using AutoMapper;
using GigHub.DTO;
using GigHub.Models;

namespace GigHub.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ApplicationUser, UserDto>();
            CreateMap<Gig, GenreDto>();
            CreateMap<Notification, NotificationDto>();
        }

    }
}
﻿using GigHub.Models;
using System.Collections.Generic;

namespace GigHub.ViewModels
{
    public class GigsViewModel
    {
        public IEnumerable<Gig> UpComingGig { get; set; }
        public bool ShowAction { get; set; }
        public string Heading { get; set; }
    }
}
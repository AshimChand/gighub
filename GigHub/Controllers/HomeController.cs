﻿using GigHub.Models;
using GigHub.ViewModels;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace GigHub.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;
        public HomeController()
        {
            _context = new ApplicationDbContext();
        }
        public ActionResult Index()
        {
            var upcominggigs = _context.Gigs
                .Include(a => a.Artist)
                .Include(a => a.Genre)
                .Where(a => a.DateTime > DateTime.Now && !a.IsCanceled).ToList();
            var viewModel = new GigsViewModel
            {
                UpComingGig = upcominggigs,
                ShowAction = User.Identity.IsAuthenticated,
                Heading="UpComing Gigs"
            };
            return View("Gigs",viewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


    }
}